SELECT Company,
       CASE WHEN Company_Response_to_Consumer='Closed' THEN count(Company_Response_to_Consumer)
           END AS Closed,
       CASE WHEN Company_Response_to_Consumer='Closed with explanation' THEN count(Company_Response_to_Consumer)
           END AS Closed_with_Explanation,
       CASE WHEN Company_Response_to_Consumer='Closed with non-monetary relief' THEN count(Company_Response_to_Consumer)
           END AS Closed_with_non_monetary_relief
FROM `consumer-complaints`
GROUP BY Company, Company_Response_to_Consumer;
