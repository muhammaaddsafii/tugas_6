SELECT MONTH(Date_Received), COUNT(Date_Received)
FROM `consumer-complaints`
GROUP BY MONTH(Date_Received);